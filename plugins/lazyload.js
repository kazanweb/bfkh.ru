import Vue from 'vue';

Vue.directive('lazyload', {
	inserted: el => {

		// Require the polyfill before requiring any other modules.
		require('intersection-observer');

		let isMobile = window.matchMedia('(max-width: 768px)').matches;

		function loadImage() {
			if (isMobile) {
				el.dataset.mSrc ? el.src = el.dataset.mSrc : el.src = el.dataset.src;
			} else {
				el.src = el.dataset.src;
			}
		}

		function callback(entries, observer) {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					loadImage();
					observer.unobserve(el);
				}
			});
		}

		function createInterserctionObserver() {

			const options = {
				root: null,
				threshold: 0
			}

			const observer = new IntersectionObserver(callback, options);

			observer.observe(el);

		}

		createInterserctionObserver();

	}
});