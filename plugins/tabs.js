import Vue from 'vue'
import tabs from '@/components/tabs/tabs/tabs'
import tab from '@/components/tabs/tabs/tab'

Vue.component('tabs', tabs)
Vue.component('tab', tab) 