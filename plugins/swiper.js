import Vue from 'vue'
import 'swiper/dist/css/swiper.css';

import SwiperWrap from '@/components/swiper/swiper/swiper.vue';
import SwiperSlide from '@/components/swiper/swiper/swiper-slide.vue';

const Swiper = require('swiper');
let swiperInitElements = [];
let options;

Vue.directive('swiper', {
	bind: function (el, binding, vnode) {

		options = binding.value;

	},
	inserted: function (el) {

		let length = el.querySelectorAll('.swiper-slide').length;
		let parent = el.parentNode;

		options.pagination = {
			el: parent.querySelector('.js-swiper__pagination'),
			clickable: true
		};

		options.navigation = {
			nextEl: parent.querySelector('.js-swiper__next'),
			prevEl: parent.querySelector('.js-swiper__prev')
		}

		if (!options.slidesPerView) {
			if (length < 2) {
				if (options.navigation.prevEl || options.navigation.nextEl) {
					options.navigation.prevEl.style.display = 'none';
					options.navigation.nextEl.style.display = 'none';
				}
			}
		} else {
			if (length < parseInt(options.slidesPerView)) {
				if (options.navigation.prevEl || options.navigation.nextEl) {
					options.navigation.prevEl.style.display = 'none';
					options.navigation.nextEl.style.display = 'none';
				}
			}
		}

		new Swiper.default(el, options)
	}
});

Vue.component('swiper', SwiperWrap);
Vue.component('swiper-slide', SwiperSlide);